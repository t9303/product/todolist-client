interface ImportMetaEnv {
	readonly NODE_ENV: 'development' | 'production';
	readonly API_METHOD:
		| 'jsonplaceholder'
		| 'local_storage'
		| 'custom_rest'
		| 'custom_graphql'
		| 'graphql';
}

interface ImportMeta {
	readonly env: ImportMetaEnv;
}
