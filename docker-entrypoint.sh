#!/bin/sh

for file in $(find . -type f -name "bundle.*.js"); do
    sed \
        -e 's/\$/tokenization_temp_dollar/g' \
        -e 's/@tokenized_env/\$/g' \
        -i $file

    (envsubst < $file) > $file.tokenized
    mv $file.tokenized $file

    sed \
        -e 's/tokenization_temp_dollar/\$/g' \
        -i $file
done

nginx -g 'daemon off;'
