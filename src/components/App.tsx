import React from 'react';
import { useLoadTodos } from 'store';
import { useMemoizedEffect } from 'shared/hooks/useMemoizedEffect';
import Body from './Body';
import Header from './Header/component';

const App: React.FC = () => {
	const loadTodos = useLoadTodos();

	useMemoizedEffect(
		(loadTodosFunc: () => void) => {
			loadTodosFunc();
		},
		[loadTodos]
	);

	return (
		<>
			<Header />
			<Body />
		</>
	);
};

export default App;
