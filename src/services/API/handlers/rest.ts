import { IAPI, ITodo, ITodoInput, Mapper } from '@/models';

const defaultMapper: Mapper<ITodo> = (todo) => todo;

export default class RestApiHandler<T> implements IAPI {
	private mapper: Mapper<T> | Mapper<ITodo>;
	private serverUrl: string;

	constructor(serverUrl: string, mapper?: Mapper<T>) {
		this.serverUrl = serverUrl;
		this.mapper = mapper || defaultMapper;
	}

	public create = async (todo: ITodoInput): Promise<ITodo> => {
		return fetch(this.serverUrl, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(todo)
		}).then((res: Response) => res.json());
	};

	public update = async (todo: ITodo): Promise<boolean> => {
		return fetch(this.serverUrl, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(todo)
		}).then((res: Response) => res.ok);
	};

	public delete = async (todoID: string): Promise<boolean> => {
		return fetch(`${this.serverUrl}/${todoID}`, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'html/text'
			}
		}).then((res: Response) => res.ok);
	};

	public getById = async (todoID: string): Promise<ITodo> => {
		return fetch(`${this.serverUrl}/${todoID}`)
			.then((res: Response) => res.json())
			.then((todo: T & ITodo) => this.mapper(todo));
	};

	public getAll = async (): Promise<ITodo[]> => {
		return fetch(this.serverUrl)
			.then((res: Response) => res.json())
			.then((tasks: (T & ITodo)[]) =>
				tasks.map((todo: T & ITodo) => this.mapper(todo))
			);
	};
}
