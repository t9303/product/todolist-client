import {
	gql,
	NextLink,
	HttpLink,
	Operation,
	ApolloLink,
	FetchResult,
	ApolloClient,
	InMemoryCache,
	NormalizedCacheObject
} from '@apollo/client';
import { IAPI, ITodo, ITodoInput } from '@/models';

export class GraphQLHandler implements IAPI {
	public service: ApolloClient<NormalizedCacheObject>;

	public constructor(serverUrl: string) {
		const tenantLink = new ApolloLink(
			(operation: Operation, forward: NextLink) => {
				operation.setContext(({ headers = {} }) => ({
					headers: {
						...headers,
						'tenant-id': 2
					}
				}));

				return forward(operation);
			}
		);

		const httpLink = new HttpLink({
			uri: serverUrl
		});

		this.service = new ApolloClient({
			link: ApolloLink.from([tenantLink, httpLink]),
			cache: new InMemoryCache({
				addTypename: false
			})
		});
	}

	public create = async (todo: ITodoInput): Promise<ITodo> => {
		const fetchResult: FetchResult = await this.service.mutate<
			ITodoInput,
			{ createOptions: ITodoInput }
		>({
			mutation: gql`
				mutation ($createOptions: CreateTodoInput!) {
					createTodo(createInput: $createOptions) {
						id
						title
						status
					}
				}
			`,
			variables: { createOptions: todo }
		});

		return <ITodo>fetchResult.data;
	};
	public update = async (todo: ITodo): Promise<boolean> => {
		return await this.service
			.mutate<ITodo, { updateOptions: Partial<ITodo> }>({
				mutation: gql`
					mutation ($updateOptions: UpdateTodoInput!) {
						updateTodo(updateInput: $updateOptions) {
							status
						}
					}
				`,
				variables: { updateOptions: todo }
			})
			.then((res) => !!res.data);
	};
	public delete = async (todoID: string): Promise<boolean> => {
		return await this.service
			.mutate<ITodo>({
				mutation: gql`
					mutation ($id: String!) {
						deleteTodo(id: $id)
					}
				`,
				variables: { id: todoID }
			})
			.then((res) => !!res.data);
	};
	public getById = async (todoID: string): Promise<ITodo> => {
		const {
			data: { todoById }
		} = await this.service.query<{ todoById: ITodo }>({
			query: gql`
				query {
					todoById {
						id
						title
						status
						owningUserId
					}
				}
			`,
			variables: {
				todoId: todoID
			}
		});
		return todoById;
	};
	public getAll = async (): Promise<ITodo[]> => {
		const {
			data: { todos }
		} = await this.service.query<{ todos: ITodo[] }>({
			query: gql`
				query {
					todos {
						id
						title
						status
						owningUserId
					}
				}
			`
		});
		return todos;
	};
}
