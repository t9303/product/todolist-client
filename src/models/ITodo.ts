import Status from './Status';

export interface ITodoInput {
	title: string;
	status: Status;
}

export interface ITodo extends ITodoInput {
	id: string;
}

export interface IOldTodo {
	id: string;
	title: string;
	completed: boolean;
}
